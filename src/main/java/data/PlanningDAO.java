package data;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

public class PlanningDAO {
	private NamedParameterJdbcTemplate template;
	private String selFlights;
	private final static String ownCode ="TLS";

	public NamedParameterJdbcTemplate getTemplate() {
		return template;
	}

	public void setTemplate(NamedParameterJdbcTemplate template) {
		this.template = template;
	}

	public void init() {
		String delFlights = "DELETE FROM flight";
		template.getJdbcOperations().execute(delFlights);
		String delPlanes = "DELETE FROM plane";
		template.getJdbcOperations().execute(delPlanes);
	}

	public long insertPlane(String model, String immat) {
		String insPlane = "INSERT INTO plane (model, immat) VALUES (:model, :immat)";
		MapSqlParameterSource planeMap = new MapSqlParameterSource();
		planeMap.addValue("model", model);
		planeMap.addValue("immat", immat);
		GeneratedKeyHolder h = new GeneratedKeyHolder();
		template.update(insPlane, planeMap, h);
		return h.getKey().longValue();
	}

	public void insertFlight(Flight flight) {
		String insFlight = "INSERT INTO flight (departure, origin, destination, plane) VALUES (:departure, :origin, :destination, :plane)";
		MapSqlParameterSource flightMap = new MapSqlParameterSource();
		flightMap.addValue("departure", flight.getDeparture());
		flightMap.addValue("origin", flight.getOrigin());
		flightMap.addValue("destination", flight.getDestination());
		flightMap.addValue("plane", flight.getPlane());
		template.update(insFlight, flightMap);
	}

	public List<Flight> getFlightsByOrigin(String origin) {
		selFlights = "SELECT * FROM flight WHERE origin LIKE :origin";
		MapSqlParameterSource flightsByOrigin = new MapSqlParameterSource();
		flightsByOrigin.addValue("origin", origin);
		List<Flight> res = new ArrayList<Flight>();
		for (Map<String, Object> m : template.queryForList(selFlights, flightsByOrigin)) {
			res.add(new Flight(new Long((Integer) m.get("id")), ((Timestamp) m.get("departure")).toLocalDateTime(),
					(String) m.get("origin"), (String) m.get("destination"), new Long((Integer) m.get("plane"))));
		}
		return res;

	}
	
	public void addJourney(String immat, String destination, LocalDateTime dep1, LocalDateTime dep2) {
		String selPlane ="SELECT id FROM plane WHERE immat LIKE :immat";
		String insDep1 ="INSERT INTO flight (departure, origin, destination, plane) VALUES (:departure, :origin, :destination, :plane)";
		String insDep2 ="INSERT INTO flight (departure, origin, destination, plane) VALUES (:departure, :origin, :destination, :plane)";
		MapSqlParameterSource selPlaneData = new MapSqlParameterSource();
		selPlaneData.addValue("immat",immat);
		long id=(Long) template.queryForObject(selPlane, selPlaneData, Long.class);
		
		MapSqlParameterSource insDep1Data = new MapSqlParameterSource();
		insDep1Data.addValue("departure", dep1);
		insDep1Data.addValue("origin", ownCode);
		insDep1Data.addValue("destination", destination);
		insDep1Data.addValue("plane", id);
		template.update(insDep1,insDep1Data);
		
		MapSqlParameterSource insDep2Data = new MapSqlParameterSource();
		insDep2Data.addValue("departure", dep2);
		insDep2Data.addValue("origin", ownCode);
		insDep2Data.addValue("destination", destination);
		insDep2Data.addValue("plane", id);
		template.update(insDep2,insDep2Data);
		
		
	
	}

}
