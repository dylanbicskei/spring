package data;

import org.springframework.data.repository.CrudRepository;

import fr.greta.bicskei.Balloon;

public interface BalloonRepository extends CrudRepository <Balloon, Integer>{
	Iterable<Balloon> findByName(String n);
	Iterable<Balloon> findBySizeGreaterThanOrderByName(double s);
	
	
	

}
