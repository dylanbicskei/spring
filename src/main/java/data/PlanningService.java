package data;

import java.time.LocalDateTime;

public class PlanningService {
	private PlanningDAO dao;
	public void execute() {
		System.out.println("Planning:");
		long p1=dao.insertPlane("Le Calisseur","T4B4RN4K");
		long p2=dao.insertPlane("l'Ostie","C4L155");
		long p3=dao.insertPlane("L'Erable","S1R0P");
		dao.insertFlight( new Flight(
				LocalDateTime.of(2019,04,03,15,55), "TLS","MRL",p1));
		dao.insertFlight( new Flight(
				LocalDateTime.of(2019,05,12,01,55), "MRS","BUD",p2));
		dao.insertFlight( new Flight(
				LocalDateTime.of(2019,05,12,01,55), "MRS","BUD",p3));
	System.out.println(dao.getFlightsByOrigin("MRS"));
	
	dao.addJourney("T4B4RN4K", "MRL", LocalDateTime.of(2019, 11,11,00,00),LocalDateTime.of(2019, 12,11,00,00));
	}
	
	public PlanningDAO getDao() {
		return dao;
	}
	public void setDao(PlanningDAO dao) {
		this.dao = dao;
	}

}
