package data;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.gemfire.repository.config.EnableGemfireRepositories;
import org.springframework.stereotype.Service;

import fr.greta.bicskei.Balloon;

@EnableGemfireRepositories("data")
@Service("balloonService")
public class BalloonService {
	private BalloonRepository repo;

	public BalloonRepository getRepo() {
		return repo;
	}

	@Autowired
	public void setRepo(BalloonRepository repo) {
		this.repo = repo;
	}

	public void execute() {
		System.out.println("Montgolfières:");
		System.out.println(repo);
		repo.save(new Balloon(800, "UnBallon", 5, 3800));
		repo.save(new Balloon(400, "UnAutreBallon", 9, 3900));
		//ArrayList <Balloon> balloonList = new ArrayList<Balloon>((Collection<? extends Balloon>) repo.findAll());
		
		//System.out.println(balloonList.size());
		for (Balloon b :repo.findAll()) {
			b.setAltitude(b.getAltitude() / 3200);
			repo.save(b);
			b.sizePrint();
		}
	}

}
