package fr.greta.bicskei;

public class Plane {
	private String model;
	private boolean available;

	public Plane() {
		this("plane", "plane", "plane");

	}

	public Plane(String plane1, String plane2, String plane3) {
		this.model = plane1;
		this.model = plane2;
		this.model = plane3;
	}

	public void init() {
	System.out.println("initiated");
	}

	public void destroy() {
	System.out.println("destroyed");
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean isAvailable) {
		this.available = isAvailable;
	}

	@Override
	public String toString() {
		return "Le modèle d'avion " + model + availability();
	}
	
	public String availability() {
		if (available == true) {
			return " disponible";
			
		}
		else return " n'est pas disponible";

	}

}
