package fr.greta.bicskei;

import java.time.LocalDateTime;

import org.aspectj.lang.JoinPoint;

public class LogAspect {
	public void enter(JoinPoint jp) {
		System.out.println(LocalDateTime.now()+"enter"+jp.getSignature().getDeclaringTypeName());
	}

	public void ahoy() {
		System.out.println("ahoy");
	}
}
