package fr.greta.bicskei;

import java.time.LocalDateTime;
import java.util.Set;

public abstract class Carrier {
	String name;
	LocalDateTime since;
	Coordinate office;
	Set<Plane> planes;

	public Set<Plane> getPlanes() {
		return planes;
	}

	public void setPlanes(Set<Plane> planes) {
		this.planes = planes;
	}

	public Coordinate getOffice() {
		return office;
	}

	public void setOffice(Coordinate office) {
		this.office = office;
	}

	public LocalDateTime getSince() {
		return since;
	}

	public void setSince(LocalDateTime year) {
		this.since = year;
	}

	public Carrier() {
		this("nom");
	}

	public Carrier(String nom) {
		this.name = nom;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Carrier name=" + name + "since :" + since + "\ncoordonnées : " + office + "\navions : " + planes + " valable le "+ getNow();
	}
	public abstract LocalDateTime getNow();

}
