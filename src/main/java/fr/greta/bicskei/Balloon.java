package fr.greta.bicskei;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.gemfire.mapping.annotation.Region;

@Region("balloonRegion")
@Entity
public class Balloon {
	public Integer id;
	public String name;
	public double size;
	public double altitude;

	public Balloon(Integer id, String name, double size, double altitude) {
		this.id = id;
		this.name = name;
		this.size = size;
		this.altitude = altitude;
	}

	public Balloon() {
		this(0, "leBallonQuiVole", 0, 0);
	}

	@Id
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	@Column
	public void setName(String name) {
		this.name = name;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	@Override
	public String toString() {
		return "Balloon \nidentifiant : " + id + "\nnom : " + name + "\ntaille : "
				+ size + "\naltitude : " + altitude;
	}

	public void sizePrint() {
		if (this.size<25) {System.out.println(
				"                          _ .--.\r\n" + "                                ( `    )\r\n"
		+ "                             .-'      `--,\r\n"
		+ "                  _..----.. (             )`-.\r\n"
		+ "                .'_|` _|` _|(  .__,           )\r\n"
		+ "               /_|  _|  _|  _(        (_,  .-'\r\n"
		+ "              ;|  _|  _|  _|  '-'__,--'`--'\r\n" + "              | _|  _|  _|  _| |\r\n"
		+ "          _   ||  _|  _|  _|  _|\r\n" + "        _( `--.\\_|  _|  _|  _|/\r\n"
		+ "     .-'       )--,|  _|  _|.`\r\n" + "    (__, (_      ) )_|  _| /\r\n"
		+ "   jgs`-.__.\\ _,--'\\|__|__/\r\n" + "                    ;____;\r\n"
		+ "                     \\YT/\r\n" + "                      ||\r\n" + "                     |\"\"|\r\n"
		+ "                     '=='" + "Balloon \nidentifiant : " + this.id + "\nnom : " + this.name + "\ntaille : "
		+ this.size + "\naltitude : " + this.altitude);
 
	
}
else System.out.println("            ,........,.......,\r\n" + "               _,/'        |         '\\,_\r\n"
		+ "           _,/'            |            '\\,_\r\n"
		+ "        ,/'                |                '\\,\r\n"
		+ "       ;                   |                   ;\r\n"
		+ "      ;                    |                    ;\r\n"
		+ "     ;                     |                     ;\r\n"
		+ "    ;                      |                      ;\r\n"
		+ "   ;                       |                       ;\r\n"
		+ "  ;                        |                        ;\r\n"
		+ "  |                        |                        |\r\n"
		+ "  ;                       / \\                       ;\r\n"
		+ "  ;                      /   \\                      ;\r\n"
		+ "   ;                    /     \\                    ;\r\n"
		+ "    ;                  /       \\                  ;\r\n"
		+ "     ;                /         \\                ;\r\n"
		+ "      '.             /           \\             .'\r\n"
		+ "       '\\,          /             \\          ,/'\r\n"
		+ "         '\\,       /               \\       ,/'\r\n"
		+ "           '\\,    /                 \\    ,/'\r\n"
		+ "             '\\, /                   \\ ,/'\r\n" + "               '\\,                   ,/'\r\n"
		+ "                 '\\,               ,/'\r\n"
		+ "                   '\\,           ,/'   _-~-~-~-~-_\r\n"
		+ "                     '\\,.......,/'   .' 0 to 60 in'.\r\n"
		+ "                       :   #   :    ( seven seconds!)\r\n"
		+ "                       : _/_\\_ :  O  '----~---~----'\r\n" + "                       :/   : \\:o\r\n"
		+ "                       | O  \\O |\r\n" + "                       |/H\\  H\\|\r\n"
		+ "                       |=======|\r\n" + "                       |=======|\r\n"
		+ "                       |_______|" + "Balloon \nidentifiant : " + this.id + "\nnom : " + this.name
		+ "\ntaille : " + this.size + "\naltitude : " + this.altitude);
};
		
	}


