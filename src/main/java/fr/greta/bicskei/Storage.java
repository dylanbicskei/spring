package fr.greta.bicskei;


import java.util.ArrayList;
import java.util.List;

public class Storage {
	
	private List<Double> volumes; 

	public Storage () {
		// TODO Auto-generated method stub
	 volumes = new ArrayList<Double>();
	}

	public int getNumber() {
		return this.volumes.size();
	}

	public List<Double> getVolumes() {
		return volumes;
	}

	public void setVolumes(List<Double> volumes) {
		this.volumes = volumes;
	}
	
	public void addVolume(double volume) {
		this.volumes.add(volume);
	}
	
	@Override
	public String toString() {
		return "volumes= " + volumes;
	}

}
