package fr.greta.bicskei;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;

import data.BalloonService;
import data.PlanningService;

class Main {

	public static void main(String[] args) {
//		System.out.println("Bienvenu au Québec \n");
		AbstractApplicationContext context = new FileSystemXmlApplicationContext("classpath:context.xml");
//		Resource ad = context.getResource("classpath:ad.txt");
//		try {
//			File adfile = ad.getFile();
//			BufferedReader r = new BufferedReader(new FileReader(adfile));
//			String l;
//			while ((l = r.readLine()) != null)
//				System.out.print(l + "\n");
//
//			r.close();
//
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
//		Coordinate c1 = (Coordinate) context.getBean("towerCoord");
//		Coordinate c2 = (Coordinate) context.getBean("Sirop_d'AirAble_Coord");
//		String towerName = (String) context.getBean("towerName");
//		Float hangarSize = (Float) context.getBean("hangarSize");
//		Storage storage = (Storage) context.getBean("storage");
//		Plane plane = (Plane) context.getBean("Avions");
//		plane.setAvailable(false);
//		plane.setModel("Cessna");
//		LocalDateTime date = (LocalDateTime) context.getBean("Date");
//		Plane plane2 = (Plane) context.getBean("Avions");
//		Carrier porteavion= (Carrier) context.getBean("carrierName");
//		
//		System.out.println("\nc1= " + c1 + " Compagnie : " + towerName + " créée le " + date + " Taille du hangar :  "
//				+ hangarSize + " m². " + storage + " " + plane);
//		System.out.println(plane2);
//		System.out.println(porteavion);
//		
//		PlanningService pls=(PlanningService) context.getBean("planningService");
//		pls.execute();
//		
		BalloonService bls=(BalloonService) context.getBean("balloonService");
		bls.execute();
	}

}
